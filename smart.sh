#!/bin/bash

# Variables
workdir=/opt/scripts/zabbix/smart
resultdir=$workdir/result
zbxdiscovery=$resultdir/zbxdiscovery

# Begin discovery file
mkdir -p $resultdir
echo "{\"data\":[" > $zbxdiscovery

# Check SMART on all disks
for disk in `lsblk |grep disk |awk '{print $1}'`; do
  echo "{ \"{#DISK}\":\"$disk\" }, " >> $zbxdiscovery
  /usr/sbin/smartctl -H /dev/$disk | grep PASSED | wc -l > $resultdir/health_${disk}
  fullinfo=`/usr/sbin/smartctl -a /dev/$disk`
  echo "${fullinfo}" | grep -i "Device Model" | cut -f2 -d: | tr -d " " > $resultdir/model_${disk}
  echo "${fullinfo}" | grep -i "Serial Number" | cut -f2 -d: | tr -d " " > $resultdir/serial_${disk}
  echo "${fullinfo}" | grep -i "Temperature_Celsius" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/temperature_${disk}
  echo "${fullinfo}" | grep -i "Power_On_Hours" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/power_hours_${disk}
  echo "${fullinfo}" | grep -i "UDMA_CRC_Error" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/crc_errors_${disk}
  echo "${fullinfo}" | grep -i "5 Reallocated" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/reallocated_sectors_${disk}
  echo "${fullinfo}" | grep -i "Current_Pending_Sector" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/pending_sectors_${disk}
  echo "${fullinfo}" | grep -i "Offline_Uncorrectable" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/offline_sectors_${disk}
  echo "${fullinfo}" | grep -i "Spin_Retry_Count" | tail -1 | cut -c 88- | cut -f1 -d' ' > $resultdir/spin_retry_${disk}
done

# Finish discovery file
echo "]}" >> $zbxdiscovery
zbxdiscoverytmp=`cat $zbxdiscovery |tr -d '\r\n' |sed 's/\(.*\),/\1/'`
echo $zbxdiscoverytmp > $zbxdiscovery
